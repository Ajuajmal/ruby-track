#Parallel Assignment

a,b,c=20,40,60 #assigning parallel method
puts "a=#{a} \nb=#{b} \nc=#{c}"

=begin
Parallel assignment is also useful for swapping the values held in two variables:
a, b = b, a
=end

#swapping
puts "\n Value before Swapping \na=#{a} \nb=#{b}"
a,b = b,a
puts "\n Value after Swapping \na=#{a} \nb=#{b}"
