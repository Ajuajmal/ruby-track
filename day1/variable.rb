#Variable Initialisation
=begin
Variable names may consist of alphanumeric characters
and the underscore character (_),
but cannot begin with a capital letter or a number
=end
x=8 #var Initialisation
puts x #ouput

#Constant Variables

=begin
Variables beginning
with a capital letter are called constants.
The value of a constant variable cannot
be changed once it has been assigned.
=end

Pi=3.14
print Pi #const variable

#Data Types

=begin
All variables in Ruby can be of all data types.
Ruby automatically determines data type
by the value assigned to the variable.
=end

x = 42 # integer
y = 1.58 # floating point value
z = "Hello" # string

puts x
puts y #output
puts z



#Print a varible inside a string

print "The value of pi is #{Pi}"
